#pragma once
#include <string>
#include <vector>

#define DL_DIRS 0b01
#define DL_FILES 0b10


typedef std::vector<std::wstring> stringvec;

static class DirLister {
	public:
		DirLister();
		~DirLister();

		static void ListDir(std::wstring path, char flags, stringvec * list);


	private:
		stringvec* list;
};

