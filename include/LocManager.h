#include <string>
#include <set>
#include <map>
#include <filesystem>
#include "ExcelFormat.h"

namespace fs = std::experimental::filesystem;
using namespace ExcelFormat;

typedef std::vector<std::wstring> stringvec;
typedef std::set<std::wstring> stringset;
typedef std::map<std::wstring, std::wstring> stringmap;
typedef std::map<std::wstring, stringmap> localizationmap;

class LocManager
{
public:
	LocManager();
	~LocManager();

	void Import(std::wstring locFoldersPath, std::wstring xlsPath);
	void Export(std::wstring xlsPath, std::wstring locFoldersPath, bool stub);

private:
	stringset xmlSet;
	localizationmap localizationMap;
	
	stringvec fileList;
	stringvec langList;
	stringvec quotesTxtList;
	stringset keys;
	int langOffset;
	int keyOffset;

	const std::locale empty_locale = std::locale::empty();
	const std::codecvt_utf8<wchar_t>* converter = new std::codecvt_utf8<wchar_t>();
	const std::locale utf8_locale = std::locale(empty_locale, converter);

	std::wstring GetWStringFromCell(BasicExcelWorksheet* sheet, int row, int col);
	std::wstring GetSheetName(BasicExcelWorksheet * sheet);
};
