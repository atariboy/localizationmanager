#include <iostream>
#include <fstream>
#include <regex>
#include "LocManager.h"
#include "DirLister.h"
#include "ExcelFormat.h"
#include "StringConv.h"

using namespace std;
using namespace ExcelFormat;

LocManager::LocManager()
{
}

LocManager::~LocManager()
{
}

std::wstring ConcatenatePath(std::wstring s1, std::wstring s2) {
	fs::path p1;
	fs::path p2;
	fs::path fullPath;

	p1 = s1;
	p2 = s2;
	fullPath = p1 / p2;
	return fullPath;
}

//Checks if string ends with another string
bool StringHasEnding(std::wstring const &fullString, std::wstring const &ending) {
	if (fullString.length() >= ending.length()) {
		return (0 == fullString.compare(fullString.length() - ending.length(), ending.length(), ending));
	}
	else {
		return false;
	}
}


std::wstring TrimQuotes(std::wstring str) {
	while ((str[0] == L'"') && (str[str.length() - 1] == L'"')) {
		str.erase(0, 1);
		str.erase(str.length() - 1, 1);
	}

	return str;
}

std::wstring TrimSpaces(std::wstring str) {
	while (str[0] == L' ') {
		str.erase(0, 1);
	}

	while (str[str.length() - 1] == L' ') {
		str.erase(str.length() - 1, 1);
	}

	return str;
}

std::wstring replaceSubstringW(const wchar_t* str, const wchar_t* find, const wchar_t* replace)
{
	std::wstring const text(str);
	std::wregex const reg(find);

	std::wstring swap_str(replace);

	return std::regex_replace(text, reg, swap_str);
}

void LocManager::Import(std::wstring locFoldersPath, std::wstring xlsPath) {
	std::wifstream fileStream;

	wstring line;
	std::wsmatch match;
	std::wstring fullPath;

	std::wstring quote_body;
	std::wstring quote_author;
	std::wstring quote_id;

	//Regex to parse key and value from xml string
	const std::wregex re(L"<\s*[^\"]+\"([^\"]+)\"\s*>([^<]+)<[^>]*>");

	BasicExcel xls;
	BasicExcelWorksheet* sheet;

	fileStream.imbue(utf8_locale);
	DirLister::ListDir(locFoldersPath, DL_DIRS, &langList);

	//Get all possible xml names
	for (stringvec::iterator lang = langList.begin(); lang != langList.end(); lang++) {
		DirLister::ListDir(ConcatenatePath(locFoldersPath, *lang), DL_FILES, &fileList);
		for (stringvec::iterator file = fileList.begin(); file != fileList.end(); file++) {
			if (StringHasEnding(*file, wstring(L"xml"))) {
				xmlSet.insert(*file);
			}
		}
	}

	//Trying to read all xmls one by one and regex_search key/value pairs
	for (stringset::iterator xmlFile = xmlSet.begin(); xmlFile != xmlSet.end(); xmlFile++) {
		sheet = xls.AddWorksheet((*xmlFile).c_str());
		keys.clear();
		localizationMap.clear();

		for (stringvec::iterator lang = langList.begin(); lang != langList.end(); lang++) {
			fullPath = ConcatenatePath(locFoldersPath, *lang);
			fullPath = ConcatenatePath(fullPath, *xmlFile);

			fileStream.open(fullPath);
			std::wcout << fullPath;
			if (fileStream.good()) {
				std::cout << " - OK" << endl;

				while (getline(fileStream, line)) {
					if (regex_search(line, match, re)) {
						localizationMap[*lang][match[1]] = match[2];
						keys.insert(match[1]);
					}
				}
			} else {
				std::cout << " - No such file" << endl;
			}

			fileStream.close();
		}

		//Writing header with languages
		langOffset = 1;
		for (stringvec::iterator lang = langList.begin(); lang != langList.end(); lang++) {
			sheet->Cell(0, langOffset)->SetWString((*lang).c_str());
			langOffset++;
		}

		//Writing keys and values
		keyOffset = 1;
		for (stringset::iterator key = keys.begin(); key != keys.end(); key++) {
			langOffset = 1;
			sheet->Cell(keyOffset, 0)->SetWString((*key).c_str());

			for (stringvec::iterator lang = langList.begin(); lang != langList.end(); lang++) {
				sheet->Cell(keyOffset, langOffset)->SetWString(localizationMap[*lang][*key].c_str());
				langOffset++;
			}

			keyOffset++;
		}

	}

	std::cout << "Quotes:" << endl;

	//Get all quotes txt names
	for (stringvec::iterator lang = langList.begin(); lang != langList.end(); lang++) {
		DirLister::ListDir(*lang, DL_FILES, &fileList);
		for (stringvec::iterator file = fileList.begin(); file != fileList.end(); file++) {
			if (StringHasEnding(*file, wstring(L"txt")) && (*file).find(L"quotes") != wstring::npos) {
				fullPath = ConcatenatePath(*lang, *file);
				quotesTxtList.push_back(fullPath);
				sheet = xls.AddWorksheet((*lang + L"_quotes").c_str());
				sheet->Cell(0, 0)->SetWString((*file).c_str());

				std::wcout << fullPath;

				fileStream.open(fullPath);

				if (fileStream.good()) {
					std::cout << " - OK" << endl;
					int row = 1;

					while (getline(fileStream, line)) {
						int col = 0;
						int symbol = 0;
						bool isCommaUncommented = true;
						int idxStart = 0;
						int idxEnd = 0;

						while (symbol <= line.length())
						{
							if (line[symbol] == '"') {
								isCommaUncommented = !isCommaUncommented;
							}
							if (line[symbol] == ',' || symbol == line.length()) {
								if (isCommaUncommented) {
									idxEnd = symbol - 1;
									std::wstring current_str = line.substr(idxStart, idxEnd - idxStart + 1);
									current_str = TrimSpaces(current_str);
									current_str = TrimQuotes(current_str);
									current_str = TrimSpaces(current_str);
									sheet->Cell(row, col)->SetWString(current_str.c_str());
									idxStart = idxEnd + 2;
									col++;
								}
							}
							symbol++;
						}

						row++;
					}
				}

				fileStream.close();
			}
		}
	}

	fs::path fsXlsPath = xlsPath;
	if (!fs::exists(fsXlsPath.parent_path())) {
		fs:create_directories(fsXlsPath.parent_path());
	}

	xls.SaveAs(xlsPath.c_str());
	std::wcout << xlsPath << " saved" << endl;
}


void LocManager::Export(std::wstring xlsPath, std::wstring locFoldersPath, bool stub) {
	BasicExcel		xls;
	wstring			lang;
	wofstream		fileStream;
	wstring			key;
	wstring			value;

	xls.Load(xlsPath.c_str());

	for (int sheetIdx = 0; sheetIdx < xls.GetTotalWorkSheets(); sheetIdx++) {
		BasicExcelWorksheet* sheet = xls.GetWorksheet(sheetIdx);

		wstring sheetName = GetSheetName(sheet);

		if (StringHasEnding(sheetName, L".xml")) {
			for (int col = 1; col < sheet->GetTotalCols(); col++) {
				lang = GetWStringFromCell(sheet, 0, col);
				lang = ConcatenatePath(locFoldersPath, lang);
				if (!fs::exists(lang)) {
					fs::create_directories(lang);
				}

				std::wstring fullPath = ConcatenatePath(lang, sheetName);
				fileStream.open(fullPath);

				if (fileStream.good()) {
					fileStream.imbue(utf8_locale);

					std::wcout << L"Writing " << fullPath << " ";

					fileStream << L"<?xml version=\"1.0\" encoding=\"utf-8\" ?>" << endl;
					fileStream << L"<Localization>" << endl;

					for (int row = 1; row < sheet->GetTotalRows(); row++) {
						key = GetWStringFromCell(sheet, row, 0);
						value = GetWStringFromCell(sheet, row, col);

						if (stub && value == L"") {
							value = key;
						}

						fileStream << L"    <String name=\"" << key << L"\">" << value << L"</String>" << endl;
					}

					fileStream << L"</Localization>";
				}
				fileStream.close();

				std::cout << "done" << endl;
			}
		}

		if (StringHasEnding(sheetName, L"quotes")) {
			lang = sheetName.substr(0, sheetName.find_last_of(L'_'));
			lang = ConcatenatePath(locFoldersPath, lang);

			std::wstring fileName = GetWStringFromCell(sheet, 0, 0);

			std::wstring fullPath = ConcatenatePath(lang, fileName);
			fileStream.open(fullPath);



			if (fileStream.good()) {
				wcout << fullPath << endl;

				std::wstring line;
				for (int row = 1; row < sheet->GetTotalRows(); row++) {
					line = L"";
					for (int col = 0; col < sheet->GetTotalCols(); col++) {
						if (TrimSpaces(GetWStringFromCell(sheet, row, col)) != L"") {
							if (col > 0) line += L'"';
							wstring cellData = GetWStringFromCell(sheet, row, col);
							cellData = TrimSpaces(cellData);
							cellData = TrimQuotes(cellData);
							cellData = TrimSpaces(cellData);
							cellData = replaceSubstringW(cellData.c_str(), L"\"", L"\"\"");
							line += cellData;
							if (col > 0) line += L'"';
							line += L',';
						}
					}

					line.erase(line.length() - 1);
					fileStream << line << endl;
				}
			}

			fileStream.close();
		}
	}



}



std::wstring LocManager::GetWStringFromCell(BasicExcelWorksheet* sheet, int row, int col) {
	wstring result = L"";

	if (sheet->Cell(row, col)->GetWString() != NULL) {
		result = sheet->Cell(row, col)->GetWString();
	} else if (sheet->Cell(row, col)->GetString() != NULL) {
		result = StringConv::ToWstring(sheet->Cell(row, col)->GetString());
	}

	return result;
}

std::wstring LocManager::GetSheetName(BasicExcelWorksheet* sheet) {
	wstring result = L"";

	wchar_t wSheetName[32];
	char sheetName[32];

	if (sheet->GetSheetName(wSheetName)) {
		result = wSheetName;
	}
	else if (sheet->GetSheetName(sheetName)) {
		result = StringConv::ToWstring(sheetName);
	}

	return result;
}

