#include "ExcelFormat.h"
#include "XGetopt.h"
#include "LocManager.h"
#include "StringConv.h"

int main(int argc, char** argv) {
	char c;
	bool exp = false;
	bool imp = false;

	bool stub = false;
	wstring locFoldersPath = L".";
	wstring xlsPath = L"localization.xls";

	while ((c = getopt(argc, argv, "eisx:l:")) != -1) {
		switch (c) {
      		case 'i':
				imp = true;
				break;
			case 'e':
				exp = true;
				break;
			case 'x':
				xlsPath = StringConv::ToWstring(optarg);
				break;
			case 'l':
				locFoldersPath = StringConv::ToWstring(optarg);
				break;
			case 's':
				stub = true;
				break;
			default:
				cout << "unknown option -" << c << endl;
		}
	}

	LocManager* locManager = new LocManager();

	if ((!imp && !exp) || (imp && exp)) {
		cout << "Please, select either import or export mode" << endl;
	} else {
		if (imp) {
			locManager->Import(locFoldersPath, xlsPath);
		}
		else {
			locManager->Export(xlsPath, locFoldersPath, stub);
		}
	}

	delete(locManager);

	getchar();
}

