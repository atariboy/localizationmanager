#include "DirLister.h"
#include "dirent.h"

DirLister::DirLister()
{
}


DirLister::~DirLister()
{
}

void DirLister::ListDir(std::wstring path, char flags, stringvec * list) {
	struct wdirent* drnt;
	WDIR* directory;
	
	list->clear();

	directory = wopendir(path.c_str());

	if (directory) {
		drnt = wreaddir(directory);
		while (drnt = wreaddir(directory)) {
			if ((flags & DL_DIRS && drnt->d_type == DT_DIR) ||
				(flags & DL_FILES && drnt->d_type != DT_DIR)) {
				list->push_back(drnt->d_name);
			}

		}

		if (flags & DL_DIRS) {
			list->erase(list->begin());
		}
	}

}