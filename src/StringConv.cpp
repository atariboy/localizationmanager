#include "StringConv.h"

StringConv::StringConv()
{
}


StringConv::~StringConv()
{
}

std::wstring StringConv::ToWstring(const char *str) {
	int max_len = strlen(str);
	std::wstring wstr;
	wstr.resize(max_len);
	MultiByteToWideChar(CP_ACP, 0, str, -1, (LPWSTR)wstr.c_str(), max_len);
	return wstr;
}